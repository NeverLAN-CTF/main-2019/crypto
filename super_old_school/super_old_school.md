# Crypto	 

### Name
Super Old School

#### Description
This image is trying to tell you something.

#### Flag
flag{so_many_cryptos}
flag{So_Many_Cryptos}
flag{somanycryptos}
flag{SOMANYCRYPTOS}
flag{s0_many_crypt0s}


#### Hints
First appeared around 2000 BC


### Internal Note
Babylonian numerals to Number letter cipher 




Your flag is so many cryptos
25-15-21-18 6-12-1-7 9-19 19-15 13-1-14-25 3-18-25-16-20-15-19

25 15 21 18 6 12 1 7 9 19 19 15 13 1 14 25 3 18 25 16 20 15 19
